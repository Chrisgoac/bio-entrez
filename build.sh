#!/bin/bash

if ! command -v docker &> /dev/null; then
    sudo apt update
    sudo apt install -y docker.io
fi

docker build -t xtec/bio-entrez .