# Entrez

Aquesta aplicació està disponible a [https://bio-entrez.azurewebsites.net/](https://bio-entrez.azurewebsites.net/).

La pots executar al teu ordinador amb `docker`:

```sh
docker run --rm -d --name entrez -p 80:80 registry.gitlab.com/xtec/bio-entrez
```

## Develop

Primer has de crear l'entorn de treball:

```sh
./init.sh
source ~/.bashrc
```

Executa l'aplicació:

```sh
npm run dev
```

Per construir una imatge

```sh
./build.sh
```



